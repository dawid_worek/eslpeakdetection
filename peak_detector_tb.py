from myhdl import *
import matplotlib.pyplot as plt
from peak_detector import peak_detector
import random
randrange = random.randrange

# Number of samples
SAMPLES_NUMBER = 1024

# Maximal value of "normal" samples
SAMPLES_RANGE = 10

# Probability of peak occurance
PEAK_PROBABILITY = 0.01

# Minimal value of peak
PEAK_MIN = 70

# Maximal value of peak
PEAK_MAX = 100

# List of input data
inputData = []

# List of indexes of peaks
results = []

@block
def peak_detector_tb():
    
    # Signal for input data
    din = Signal(modbv(0, min=0, max=2**32))

    # Signal for output data
    dout = Signal(modbv(0, min=0, max=2**32))

    # Signal to turn on/off peak detector
    en = Signal(bool(0))

    # Signal to indicate putting data (write enable)
    wen = Signal(bool(0))

    # Signal to indicate that output is ready to be read
    outReady = Signal(bool(0))

    # Signal to indicate that no peaks were find
    noPeaks = Signal(bool(0))

    # Clock
    clk = Signal(bool(0))

    # Create instance of peak detector
    pd = peak_detector(din, dout, en, wen, outReady, noPeaks, clk)

    @always(delay(1))
    def drive_clk():
        clk.next = not clk

    @instance
    def testbench():

        # Generate input data
        for i in range(SAMPLES_NUMBER):
            if randrange(1 / PEAK_PROBABILITY) == 0:
                inputData.append( randrange(PEAK_MAX - PEAK_MIN)  + PEAK_MIN )
            else:
                inputData.append(randrange(SAMPLES_RANGE))

        yield clk.negedge # Wait for negative edge

        en.next = bool(1) # Enable peak detector
        wen.next = bool(1) # Enable writing to the peak detector

        # Write data to the peak detector
        for i in inputData:
            din.next = i
            yield clk.negedge

        wen.next = bool(0) # Disable writing

        wasData = False
        while True:
            yield clk.posedge # Wait for valid output data
            if outReady:
                wasData = True
                results.append(int(dout.val))
            elif wasData:
                
                # Plot input and output
                plt.plot(inputData)
                plt.plot(results, [inputData[i] for i in results], 'ro')
                plt.show()

                raise StopSimulation()

            elif noPeaks:

                # No peaks were detected
                print("No peaks detected")
                raise StopSimulation()

    return instances()

if __name__ == '__main__':
    tb = peak_detector_tb()
    tb.run_sim()