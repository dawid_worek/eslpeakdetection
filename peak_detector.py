from myhdl import block, Signal, always, enum, modbv
import numpy as np
from copy import copy

# 1 <= H <= 3 - the higher H, the higher peaks have to be to be detected
H = 3

# Maximal width of peak in samples
K = 5

@block
def peak_detector(din, dout, en, wen, outReady, noPeaksDetected, clk, resolution=32, widowSizeMax = 1024, minValue = 0, maxValue = 2**32):
    """
    Arguments Description

    din:             Input data
    dout:            Output data - indexes of detected peaks. 
                     When outReady is high, on every positive edge of the clock new output is ready.
    en:              Signal to turn on/off peak detector
    wen:             Write enable - putting data in
    outReady:        Signal to indicate that output is ready to be read (high level)
    noPeaksDetected: Signal to indicate that no peaks were find (positive edge)
    clk:             Clock
    """
    stateEnum = enum('COLLECTING', 'CALCULATING', 'OUTPUTTING')
    state = Signal(stateEnum.COLLECTING)

    indexCounter = Signal(modbv(0, min = 0, max = widowSizeMax))
    register_values_temp = []
    indexes = []

    a = []

    @always(clk.posedge)
    def main_imp():
        if en:

            # Finite states machine

            if state == stateEnum.COLLECTING:
                if indexCounter.val == 0:
                    noPeaksDetected.next = bool(0)

                if wen:
                    register_values_temp.append(copy(din.val))
                else:
                    indexCounter.next = 0
                    state.next = stateEnum.CALCULATING
            elif state == stateEnum.CALCULATING:
                # MAIN PROCESS

                # Calculate peak function TODO
                for i in range(K, len(register_values_temp) - K):
                    ix = i
                    value = register_values_temp[i]

                    left = max([register_values_temp[ix] - register_values_temp[i] for i in range(ix - K, ix)])
                    right = max([register_values_temp[ix] - register_values_temp[i] for i in range(ix + 1, ix + K + 1)])
                    a.append((left + right) / 2) 

                # Calculate mean
                meanValue = np.mean(a)

                # Calculate standard devation
                stdValue = np.std(a)
                
                # Detect peaks
                for ind, val in enumerate(a):
                    if (val > 0) and (val - meanValue) > (H * stdValue):
                        indexes.append(ind + K)

                # Remove peaks which are too close to each other
                for i in range(1, len(indexes)):
                    try:
                        if abs(indexes[i] - indexes[i-1]) <= K:
                            if register_values_temp[indexes[i]] > register_values_temp[indexes[i-1]]:
                                del indexes[i-1]
                            else:
                                del indexes[i]
                    except:
                        pass

                # When calculating is finished, output indexes
                state.next = stateEnum.OUTPUTTING

            elif state == stateEnum.OUTPUTTING:
                
                if indexCounter.val == len(indexes):

                    if indexCounter.val == 0:
                        noPeaksDetected.next = bool(1)
                        register_values_temp.clear()
                        indexes.clear()
                        state.next = stateEnum.COLLECTING
                    else:
                        register_values_temp.clear()
                        indexes.clear()
                        indexCounter.next = 0
                        state.next = stateEnum.COLLECTING
                        outReady.next = bool(0)
                else:
                    outReady.next = bool(1)
                    dout.next = indexes[indexCounter.val]
                    indexCounter.next = indexCounter + 1

    return main_imp