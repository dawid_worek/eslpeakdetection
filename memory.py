from myhdl import *

@block
def memory(din, dout, addr, writeEnable, enable, clock, n=32, size = 1024, minValue = 0, maxValue = 2**32):
    """
    Clasic memory.

    din:         input data - data to save
    dout:        output data - data read from the memory
    addr:        address of the data to read/save (depends on writeEnable)
    writeEnable: when true, data from din are saved under the given address. 
                 When false, data is read from the given address and put on the dout signal
    enable:      set to true when operation on the memmory is going to be performed
    clock:       just clock

    n:           number of bits of the word
    size:        maximal number of words which can be saved in the memory
    minValue:    minimal value which can be stored in the memory
    maxValue:    maximal value which can be stored in the memory
    """

    mem = [Signal(modbv(0, min = minValue, max = maxValue)) for i in range(size)]

    @always(clock.posedge)
    def logic():
        if enable:
            if writeEnable:
                mem[addr.val].next = din.val
            else:
                dout.next = mem[addr.val].val

    return logic