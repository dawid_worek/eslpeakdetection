from myhdl import *
from memory import *
from random import randrange

VALUES_TO_PUT = 16

WORD_SIZE = 32

@block
def memory_tb():
    din = Signal(modbv(0, min = 0, max = 2**WORD_SIZE))
    dout = Signal(modbv(0, min = 0, max = 2**WORD_SIZE))
    addr = Signal(modbv(0, min = 0, max = 2**WORD_SIZE))
    en = Signal(bool(0))
    wen = Signal(bool(0))
    clk = Signal(bool(0))

    @always(delay(10))
    def clock():
        clk.next = not clk

    @instance
    def stimulus():

        # Save values
        print("Saving values...")
        yield clk.negedge
        en.next = bool(1)
        wen.next = bool(1)
        addr.next = 0
        for i in range(VALUES_TO_PUT):
            din.next = randrange(2**WORD_SIZE)
            yield clk.posedge
            print("Value %d put in %d" % (int(din), int(addr)))
            addr.next = addr + 1

        # Read values
        yield clk.negedge
        print("Reading values...")
        wen.next = bool(0)  
        addr.next = 0
        yield clk.posedge
        for i in range(VALUES_TO_PUT):
            yield clk.posedge
            print("Value at %d is %d" % (int(addr), int(dout)))
            addr.next = addr + 1
            yield clk.posedge

        # Disable memory
        en.next = bool(0)
        yield clk.posedge

        raise StopSimulation()

    memInst = memory(din, dout, addr, wen, en, clk)

    return instances()

if __name__ == '__main__':
    tb = memory_tb()
    tb.run_sim()